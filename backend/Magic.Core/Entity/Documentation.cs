﻿using SqlSugar;
using System.Collections.Generic;
using System.ComponentModel;
namespace Magic.Core.Entity;

/// <summary>
/// 文档表
/// </summary>
[SugarTable("Documentation")]
[Description("文档表")]
public class Documentation : DEntityBase
{
    /// <summary>
    /// 父Id
    /// </summary>
    public long PId { get; set; }

    /// <summary>
    /// 父ID列表
    /// </summary>
    public string PIds { get; set; }
    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// 文档类型:文件、文件夹
    /// </summary>
    public DocumentType DocumentType { get; set; }
    /// <summary>
    /// 文件后缀
    /// </summary>
    public string FileSuffix { get; set; }
    /// <summary>
    /// 文件大小kb
    /// </summary>
    public int? FileSizeKb { get; set; }



    /// <summary>
    /// 文件路径
    /// </summary>
    public string FilePath { get; set; }


    /// <summary>
    /// 存储后的文件名
    /// </summary>
    public string FileObjectName { get; set; }
    /// <summary>
    /// 标签
    /// </summary>
    public int? Label { get; set; }
    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; }

    [SqlSugar.SugarColumn(IsIgnore = true)]
    public List<Documentation> Children { get; set; }

    /// <summary>
    /// 是否可见
    /// </summary>
    public bool Visible { get; set; } = true;
}
